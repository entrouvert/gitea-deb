.PHONY: clean name version fullname dist dist-bzip2

NAME=gitea
VERSION=$(shell (test -d .git && git describe || cat VERSION) | sed 's/^v//; s/-/./g')

prefix = /usr

gitea:
	python3 download.py $(VERSION)
	chmod +x gitea

all: gitea

install: gitea

clean:
	rm -f gitea

DIST_FILES = \
	COPYING \
	Makefile \
	download.py

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	echo $(VERSION) > sdist/$(NAME)-$(VERSION)/VERSION
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

install: gitea
	mkdir -p $(DESTDIR)$(prefix)/bin/
	cp -r gitea $(DESTDIR)$(prefix)/bin/

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
