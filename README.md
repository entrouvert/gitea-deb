# Paquet debian semi-automatique pour serveur Gitea

Ce dépôt contient un fichier `Jenkinsfile` qui peut être utilisé pour construire un paquet Gitea basique contenant le binaire téléchargé depuis https://dl.gitea.com/gitea/

## Utilisation

Créer un tag git correspondant au numéro de release de Gitea, par exemple `v1.19.0`. Pousser ce tag et déclencher un build Jenkins en visitant https://jenkins.entrouvert.org/job/gitea/job/gitea-deb/job/main/build?delay=0sec

Pour déployer ce paquet :

```bash
ssh git.entrouvert.org
sudo apt update
sudo apt install --only-upgrade gitea
```
