#! /usr/bin/env python3

import hashlib
import os
import subprocess
import sys

import requests

if os.path.exists('gitea'):
    os.unlink('gitea')

if len(sys.argv) > 1:
    version = sys.argv[1]
elif os.path.exists('VERSION'):
    with open('VERSION') as fd:
        version = fd.read()

version = '.'.join(version.split('.')[:3])

url = f'https://dl.gitea.com/gitea/{version}/gitea-{version}-linux-amd64.xz'
print('downloading', url)
r = requests.get(url)
if not r.ok:
    print('failed to download')
    sys.exit(1)

sha_r = requests.get(f'{url.removesuffix(".xz")}.sha256')
if not sha_r.ok:
    print('failed to download checksum')
    sys.exit(1)

sha = sha_r.text.split()[0]

with open('gitea.xz', 'wb') as fd:
    fd.write(r.content)

p = subprocess.run(['unxz', 'gitea.xz'])

with open('gitea', 'rb') as fd:
    checksum = hashlib.sha256(fd.read()).hexdigest()
    if checksum != sha:
        print(f'invalid checksum, {checksum} (expected: {sha})')
        os.unlink('gitea')
        sys.exit(1)
